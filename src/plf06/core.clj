(ns plf06.core
  (:gen-class))

(def lower-case
  (seq "aábcdeéfghiíjklmnñoópqrstuúüvwxyz"))


(def capital-letter
  "Devuelve una secuencia de las letras mayusculas"
  (seq "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ"))

(def symbols
  "Devuelve una secuencia de caracteres"
  (seq "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789"))

(defn rot-13
  [s]
  (let [element (concat symbols symbols lower-case capital-letter symbols)
        f (zipmap element (flatten (repeat (count s) (take 150 (drop 13 element)))))
        g (fn [x] (apply str (map #(get f % %) x)))
        h (fn [x]
            (cond
              (< (count x) 3) (apply str (g x))
              (= (first (rest x)) \space) (apply str (first x) (g (rest x)))
              (contains? (into #{} x) \space) (g x)
              :else
              (g (apply str (subs x 0 (- (count x) 3)) " " (subs x (- (count x) 3) (count x))))))]
    (h s)))

(defn -main
  [& args]
  (if (empty? args)
    (println "Debe tener al menos un caracter") ;; ¿A qué se debe el error? ¿Alguna forma de indicarle al usuario la razón?
    (println (rot-13 (apply str args)))))

;; pruebas
(rot-13 "Canción #72")
(rot-13 "c AN CIÓN # 7 2")
(-main "Can" "ción" "#" "72")
(-main "Canción" "#72")
(-main "Can" "ción" "#72")
(-main "Can" "ción" "#" "72")
(-main)